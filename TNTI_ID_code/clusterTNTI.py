import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from cluster import cluster
from cluster2D import cluster2D
from cluster3D import cluster3D
import pickle
from myFigs import set_size,set_square,set_stretch,set_almoststretch,set_almoststretch2,set_equal
import pdb

from matplotlib import ticker, cm

figType='.pdf'
width = 468

nice_fonts = {
        # Use LaTex to write all text
        "text.usetex": True,
        "font.family": "serif",
        # Use 10pt font in plots, to match 10pt font in document
        "axes.labelsize": 11,
        "font.size": 11,
        # Make the legend/label fonts a little smaller
        "legend.fontsize": 8,
        "xtick.labelsize": 8,
        "ytick.labelsize": 8,
}

mpl.rcParams.update(nice_fonts)

mycase='DNS'

pathexp='../database/TBL_experiment/'
pathdns='../database/TBL_transp_DNS/'
if mycase=='Lille_1':
  case='U05_X_595_r2_0005.xyz'
  filename = pathexp+case
  X,Y,Z,U,V,VORT = np.loadtxt(filename,unpack=True)
  X = np.reshape(X,(332,975)).T
  Y = np.reshape(Y,(332,975)).T
  U = np.reshape(U,(332,975)).T
  V = np.reshape(V,(332,975)).T
  dataType='exp'
if mycase=='Lille_2':
  case='U05_X_595_r2_0584.xyz'
  filename = pathexp+case
  X,Y,Z,U,V = np.loadtxt(filename,unpack=True)
  X = np.reshape(X,(332,975)).T
  Y = np.reshape(Y,(332,975)).T
  U = np.reshape(U,(332,975)).T
  V = np.reshape(V,(332,975)).T
  dataType='exp'
if mycase=="DNS":
  case='uniform3_n0309335_t6.970e+02_slice.p'
  filename = pathdns+case
  infile = open(filename,'rb')
  data = pickle.load(infile)
  print "I stored "+  str(data['nbslices']) + ". Manually slice, please!"
  x = data['x']
  y = data['y']
  X,Y=np.meshgrid(x,y,indexing='ij')
  U = data['u'][:,:,0]
  V = data['v'][:,:,0]
  W = data['w'][:,:,0]
  s=1000
  e=1800
  ny=len(y)
  dataType='dns'
else:
  case='snapshot1.dat'
  filename = path+case
  X,Y,U,V = np.loadtxt(filename,delimiter=',',unpack=True)
  X = np.reshape(X,(86,216)).T
  Y = np.reshape(Y,(86,216)).T
  U = np.reshape(U,(86,216)).T
  V = np.reshape(V,(86,216)).T

x = X[:,2]
y = Y[0,:]

# We just show how bad the threshold based methods are
show_how_bad=False
if show_how_bad:
    #compute vorticity
    dUdy=np.zeros(np.shape(U))
    dVdx=np.zeros(np.shape(U))
    vort=np.zeros(np.shape(U))
    dUdy[:,1:-1]= (U[:,2:]-U[:,:-2])/(y[3]-y[1])
    dVdx[1:-1,:]= (V[2:,:]-V[:-2,:])/(x[3]-x[1])
    vort = abs(dUdy-dVdx)
    if dataType=='exp':
      plt.figure(figsize=set_equal(width,y[-1]/x[-1]))
    else:
      plt.figure()
    print "min/max vorticity: "+str(min(vort.flatten()))+ " / "+str(max(vort.flatten()))
    plt.contour(X,Y,vort,[0.05,0.5])
    plt.contourf(X,Y,vort,250)
    plt.xlabel('z')
    plt.ylabel('y')
    plt.tight_layout()
    plt.savefig("../figs/vorticity_ID"+figType)
    plt.show()

compute_vort=True
if compute_vort:
    dUdy=np.zeros(np.shape(U))
    dVdx=np.zeros(np.shape(U))
    vort=np.zeros(np.shape(U))
    dUdy[:,1:-1]= (U[:,2:]-U[:,:-2])/(y[2:]-y[:-2])
    dVdx[1:-1,:]= (V[2:,:]-V[:-2,:])/(x[3]-x[1])
    vort = abs(dUdy-dVdx)


compute_avg=True
if compute_avg:
    Uavg=np.zeros(np.shape(U))
    Uavg[1:-1,1:-1]= U[2:,1:-1]+U[0:-2,1:-1]+U[1:-1,2:]+U[1:-1,:-2]
    Uavg/=4

compute_lamb=True
if compute_lamb:
    L=vort*U

compute_planavg=True
if compute_planavg:
    Upavg=np.zeros(np.shape(U))
    for jj in range(ny):
      mean=np.mean(U[:,jj])
      Upavg[:,jj]=mean

n_clusters = 3
cut=80
trial1 = cluster(x,y[cut:],U[:,cut:],n_clusters)
#trial1 = cluster2D(x,y[cut:],U[:,cut:]**2,vort[:,cut:],n_clusters)
#trial1 = cluster3D(x,y[cut:],U[:,cut:],V[:,cut:],W[:,cut:],n_clusters)
res=trial1.labels.T
if dataType=='exp':
  plt.figure(figsize=set_equal(width,y[-1]/x[-1]))
else:
  plt.figure()
plt.contourf(X[s:e,:],Y[s:e,:],(U[s:e,:]-1)+V[s:e,:],250,cmap='RdBu')
plt.contour(x,y[cut:],res,3,colors='k')
plt.contour(X[s:e,:],Y[s:e,:],vort[s:e,:],[0.056],colors='r')
plt.xlabel('z')
plt.ylabel('y')
plt.xlim(28,31)
plt.ylim(0,3)
plt.tight_layout()
plt.savefig("../figs/FCM_ID_U"+figType)
#plt.plot(x,trial1.ys[:,0],color='red',linestyle='--')
plt.show()
