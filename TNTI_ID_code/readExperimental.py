import netCDF4
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pickle
import pdb
#path="/Volumes/MPILAB/DATA/boundaryLayer_experimental/lml_apg_turbulent_boundary_layer_spanwise_planes/U05_X_595_r2.nc"
path="./"
fname="U05_X_595_r2"
openPath=path+fname+".nc"
f=netCDF4.Dataset(openPath)
U=f.variables['vel_x']
V=f.variables['vel_z']
z=f.variables['grid_z']
y=f.variables['grid_y']
ny,nz=np.shape(z)
Uavg=np.mean(U[:500,:,:],axis=0)
Umean=np.mean(Uavg,axis=1)
Ui=Umean[-1]

slices=[3]#,55,102,555,777,888,1000,3000,4000]
nslices=len(slices)


TKE=np.zeros([nslices,ny,nz])
homog=np.zeros([nslices,ny,nz])
ones=np.ones([ny,nz])
for ss,slice in enumerate(slices):

  #TKE threshold
  Uslice=U[slice,:,:]-Ui*ones
  Vslice=V[slice,:,:]
  TKE[ss,1:-1,1:-1]=np.log(Uslice[0:-2,0:-2]**2 +Uslice[1:-1,0:-2]**2+Uslice[2:,0:-2]**2+ Uslice[0:-2,1:-1]**2 +Uslice[1:-1,1:-1]**2+Uslice[2:,1:-1]**2 + Uslice[0:-2,2:]**2   +Uslice[1:-1,2:]**2  +Uslice[2:,2:]**2 + Vslice[0:-2,0:-2]**2 +Vslice[1:-1,0:-2]**2+Vslice[2:,0:-2]**2+    Vslice[0:-2,2:]**2   +Vslice[1:-1,2:]**2    +Vslice[2:,2:]**2)*100
  TKE[ss,:,:]/=(9*Ui*Ui)

  #Homogeneity threshold
  Uslice=U[slice,:,:]
  nsize=5
  hsize=int((nsize-1)/2)
  for jj in range(hsize,nz-hsize):
    for ii in range(hsize,ny-hsize):
      #pdb.set_trace()
      avgU=np.log(np.mean(Uslice[ii-hsize:ii+hsize,jj-hsize:jj+hsize])
      homog[ss,ii,jj]=np.sqrt(np.sum((Uslice[ii-hsize:ii+hsize,jj-hsize:jj+hsize]-avgU)**2)/(nsize**2-1)))


plt.contourf(TKE[0,:,:])
plt.figure()
plt.contour(homog[0,:,:])
plt.show()

data = { "slices": slices, "z":z , "y":y,'U':U[slices,:,:],"homog":homog,"TKE":TKE}
pickle.dump( data, open( fname+".pickle", "wb" ) )
