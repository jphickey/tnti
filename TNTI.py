import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from cluster_it import cluster_it
import scipy.io as sio

#########################################################
# Specify the path for the cases and the case name.
# Choose to save or not.
#########################################################
pathexp='database/TBL_experiment/'
mycase='UofA'
save=1

if mycase=='UofA':
	nice_fonts = {
	        "text.usetex": False,
	        "font.family": "serif",
	        "mathtext.fontset": "dejavuserif",
	        # Use 18pt font in plots to ensure correct size in paper.
	        "axes.labelsize": 18,
	        "font.size": 18,
	        # Make the legend/label fonts a little smaller.
	        "legend.fontsize": 17,
	        "xtick.labelsize": 17,
	        "ytick.labelsize": 17}
	# Specify zoomed in or out figure (in/out)
	plot='in'
	if plot=='in':
		width = 5
		height = 4
		ylim = 0.9
	if plot=='out':
		width = 3
		height = 4
		ylim = 1
	xlim = 0.4
else:
	nice_fonts = {
	    "text.usetex": False,
	    "font.family": "serif",
	    "mathtext.fontset": "dejavuserif",
	    # Use 12pt font in plots to match 12pt font in document.
	    "axes.labelsize": 12,
	    "font.size": 12,
	    # Make the legend/label fonts a little smaller.
	    "legend.fontsize": 11,
	    "xtick.labelsize": 11,
	    "ytick.labelsize": 11}
	# Specify full width or not (normal/full)
	plot='normal'
	if plot=='normal':
		width = 5
		height = 4
		xlim = 1
		ylim = 1
	if plot=='full':
		width = 10
		height = 4

# Update figure parameters
mpl.rcParams.update(nice_fonts)

#########################################################
# All variables must be matrices.
# Must reshape such that rows x columns correspond to
# y-data then x-data. Then transpose for consistency.
#########################################################
if mycase=='Adrian_High':
	# Specify snapshot
	case='AMT00High23.dat'
	filename=pathexp+'AMT00HighReAll/'+case
	X,Y,U,V = np.loadtxt(filename,unpack=True,skiprows=1,delimiter=',')
	X = np.reshape(X,(86,216)).T
	Y = np.reshape(Y,(86,216)).T
	U = np.reshape(U,(86,216)).T
	V = np.reshape(V,(86,216)).T
	x = X[:,2]
	y = Y[0,:]
	delta = 83.1
	Uinf = 11.39

if mycase=='Adrian_Low':
	# Specify snapshot
    case='AMT00Low39.dat'
    filename=pathexp+'AMT00LowReAll/'+case
    X,Y,U,V = np.loadtxt(filename,unpack=True,skiprows=1,delimiter=',')
    X = np.reshape(X,(90,216)).T
    Y = np.reshape(Y,(90,216)).T
    U = np.reshape(U,(90,216)).T
    V = np.reshape(V,(90,216)).T
    x = X[:,2]
    y = Y[0,:]
    delta = 76.2
    Uinf = 1.77

if mycase=='UofA':
    case='UofA/sampleVectors.mat'
    filename = pathexp+case
    mat_contents = sio.loadmat(filename)
    x = mat_contents['X'][0][2:-2][::-1] 
    x = x + x.max()
    y = mat_contents['Y'][::-1].T[0]
    X,Y=np.meshgrid(x,y,indexing='ij')
    # Specify snapshot
    file=-1
    U = mat_contents['U_set'][::-1,2:-2,file].T
    V = mat_contents['V_set'][::-1,2:-2,file].T
    Uall = mat_contents['U_set'][::-1,2:-2,:].T
    Vall = mat_contents['V_set'][::-1,2:-2,:].T
    delta = 93.8
    Uinf = 11.8

# Mean quantities if needed
Ubar=np.mean(U,axis=0)
Vbar=np.mean(V,axis=0)	
#########################################################
# Toggle whichever option you want
# Currently, it will overlay turbke and fuzzy on top 
# of one another
#########################################################
turbke=1
fuzzy=1

#########################################################
# Computing the TKE
#########################################################
if turbke:
	# Compute the mean at the furthest (top) y-location 
	# This is considered to be the 'freestream'
	Umean = np.mean(U[:,-4])
	TKE = np.zeros(np.shape(U))

	TKE[1:-1,1:-1] = (U[:-2,1:-1] - Umean)**2 + \
				(U[2:,1:-1] - Umean)**2 + \
				(U[1:-1,1:-1] - Umean)**2 + \
				(U[:-2,:-2] - Umean)**2 + \
				(U[2:,:-2] - Umean)**2 + \
				(U[1:-1,:-2] - Umean)**2 + \
				(U[:-2,2:] - Umean)**2 + \
				(U[2:,2:] - Umean)**2 + \
				(U[1:-1,2:] - Umean)**2 + \
				V[:-2,1:-1]**2 + V[1:-1,1:-1]**2 + V[2:,1:-1]**2 + \
				V[:-2,:-2]**2 + V[1:-1,:-2]**2 + V[2:,:-2]**2 + \
				V[:-2,2:]**2 + V[1:-1,2:]**2 + V[2:,2:]**2

	TKE = TKE*100/9/Umean**2

# Formatting plots
plt.figure(1,figsize=(width,height))
if plot=='full':
	plt.contourf(X/delta,Y/delta,U/Uinf,200,cmap='Greys_r',extend='both')
	plt.tick_params(axis='both', which='both', bottom=False, top=False, labelbottom=False, right=False, left=False, labelleft=False)
else:
	levels = np.linspace(0.5,1,200)
	plt.contourf(X/delta,Y/delta,U/Uinf,levels,cmap='Blues_r',extend='both')
	plt.ylabel('y/$\\delta$')
	plt.xlabel('x/$\\delta$')
	bounds = [0.5, 0.75, 1]
	if plot=='normal' or plot=='in':
		b = plt.colorbar(boundaries=bounds, ticks=bounds, pad=0.02)
		b.set_label(label='U/U$_{\\infty}$')
	plt.ylim(0,ylim)
	if plot=='in':
		plt.ylim(0.5,ylim)
		plt.yticks([0.5,0.6,0.7,0.8,0.9])
		plt.xticks([0,0.1,0.2,0.3,0.4])
	plt.xlim(0,xlim)

#########################################################
# This is the iterative process. 2 clusters, 2 times is
# shown. The algorithm will print out the FPC at each 
# trial. If that is severely dropping, then we are not 
# converging.
#########################################################
if fuzzy: 
	clusters = 2
	times = 2
	trial = cluster_it(x,y,U,clusters,times)
	if mycase=='UofA':
		plt.contour(X/delta,Y/delta,trial.clustSum,[1.5],colors='k',linewidths=1.125)
	else:
		plt.contour(X/delta,Y/delta,trial.clustSum,[1.5],colors='k',linewidths=0.75)

# Overlaying the TKE
if turbke:
	if mycase == 'UofA':
		plt.figure(1)
		plt.contour(X[1:-1,1:-1]/delta,Y[1:-1,1:-1]/delta,-np.log(TKE[1:-1,1:-1]),[-np.log(0.35)],\
			colors='r',linestyles=':',linewidths=1.5)
	if mycase == 'Adrian_High': 
		plt.figure(1)
		plt.contour(X[1:-1,1:-1]/delta,Y[1:-1,1:-1]/delta,-np.log(TKE[1:-1,1:-1]),[-np.log(0.68)],\
			colors='r',linestyles=':',linewidths=1.0)
	if mycase == 'Adrian_Low':
		plt.figure(1)
		plt.contour(X[1:-1,1:-1]/delta,Y[1:-1,1:-1]/delta,-np.log(TKE[1:-1,1:-1]),[-np.log(0.57)],\
			colors='r',linestyles=':',linewidths=1.0)
plt.tight_layout()

# Saving figures
if save==1:
	if mycase=='UofA':
		if plot=='in':
			plt.savefig('figs/UofAzoom.png', bbox_inches='tight', dpi=600)
		else:
			plt.savefig('figs/UofA.png', bbox_inches='tight', dpi=600)
	if mycase =='Adrian_High':
		if plot=='full':
			plt.savefig('figs/Adrian_High_Full.png', bbox_inches='tight', dpi=1200)
		else:
			plt.savefig('figs/Adrian_High.png', bbox_inches='tight', dpi=600)
	if mycase =='Adrian_Low':
		if plot=='full':
			plt.savefig('figs/Adrian_Low_Full.png', bbox_inches='tight', dpi=1200)
		else:
			plt.savefig('figs/Adrian_Low.png', bbox_inches='tight', dpi=600)
plt.show()

