import numpy as np
import skfuzzy as fuzz

class cluster_it():
    def __init__(self,x,y,U,n_clusters,nb):
        self.clustSum=np.ones(np.shape(U))
        data = np.reshape(U,(1,-1))
        cntr, u, u0, d, jm, p, fpc = fuzz.cluster.cmeans(data,n_clusters,2,error=0.00001, maxiter=10000,init=None)
        self.labels = np.reshape(np.argmax(u,axis=0),U.shape)
        self.labels1 = self.relabel(self.labels,cntr,n_clusters)
        self.fp1 = fpc
        if nb == 1:
            self.clustSum=self.labels1

        print("The fuzzy partition coeffient for the first pass with", n_clusters, "clusters is:", np.round(fpc,3))

        if nb > 1:
            data = np.reshape(U[self.labels1==n_clusters],(1,-1))
            new_clusters = n_clusters
            cntr, u, u0, d, jm, p, fpc = fuzz.cluster.cmeans(data,new_clusters,2,error=0.00001, maxiter=10000,init=None)
            self.fp2 = fpc
            
            if self.fp2 > self.fp1:
                clust=np.argmax(u,axis=0)
                self.labels2 = self.relabel2(clust,cntr,new_clusters)
                self.clustSum[self.labels1==n_clusters]=self.labels2

                print("The fuzzy partition coeffient for the second pass with", new_clusters, "clusters is:", np.round(fpc,3))
            else:
                print("Sorry, the agreement is becoming worse. I am not proceeding.")
                self.clustSum=self.labels1

            if nb > 2:
                data = np.reshape(U[self.clustSum==n_clusters],(1,-1))
                new_clusters = n_clusters
                cntr, u, u0, d, jm, p, fpc = fuzz.cluster.cmeans(data,new_clusters,2,error=0.00001, maxiter=10000,init=None)
                self.fp3 = fpc

                if self.fp3 > self.fp2:
                    clust=np.argmax(u,axis=0)
                    self.labels3 = self.relabel2(clust,cntr,new_clusters)
                    self.clustSum[self.clustSum==n_clusters]=self.labels3

                    print("The fuzzy partition coeffient for the third pass with", new_clusters, "clusters is:", np.round(fpc,3))
                else:
                    print("Sorry, the agreement is becoming worse. I am not proceeding.")
                    self.clustSum=self.labels2

                if nb > 3:
                    data = np.reshape(U[self.clustSum==n_clusters],(1,-1))
                    new_clusters = n_clusters
                    cntr, u, u0, d, jm, p, fpc = fuzz.cluster.cmeans(data,new_clusters,2,error=0.00001, maxiter=10000,init=None)
                    self.fp4 = fpc

                    if self.fp4 > self.fp3:
                        clust=np.argmax(u,axis=0)
                        self.labels4 = self.relabel2(clust,cntr,new_clusters)
                        self.clustSum[self.clustSum==n_clusters]=self.labels4
                
                        print("The fuzzy partition coeffient for the fourth pass with", new_clusters, "clusters is:", np.round(fpc,3))
                    else:
                        print("Sorry, the agreement is becoming worse. I am not proceeding.")
                        self.clustSum=self.labels3

        #LAST STEP NEEDED
        self.ys = self.get_ys(self.clustSum,y,n_clusters)

    def relabel(self,label,center,n_clusters):
        tmp = np.linspace(0,n_clusters-1,n_clusters,dtype=np.int)
        center,tmp = zip(*sorted(zip(center,tmp)))
        xx,yy = np.shape(label)
        mask = np.zeros((xx,yy,n_clusters))
        for ii in range(n_clusters):
            mask[:,:,ii] = label == tmp[ii]
        for ii in range(n_clusters):
            label[np.nonzero(mask[:,:,ii])] = ii+1
        return label

    def relabel2(self,label,center,n_clusters):
        tmp = np.linspace(0,n_clusters-1,n_clusters,dtype=np.int)
        center,tmp = zip(*sorted(zip(center,tmp)))
        xx= len(label)
        mask = np.zeros((xx,n_clusters))
        for ii in range(n_clusters):
            mask[:,ii] = label == tmp[ii]
        for ii in range(n_clusters):
            label[np.nonzero(mask[:,ii])] = ii+1
        return label

    def get_ys(self,label,y,n_clusters):
        nx,ny = label.shape

        ys = np.zeros((nx,n_clusters-1))

        for n in range(n_clusters-1):
          for ii in range(nx):
            ytmp = np.array([])
            for jj in range(ny-1):
              if (label[ii,jj] == n+2 and label[ii,jj+1] == n+1) or (label[ii,jj] == n+1 and label[ii,jj+1] == n+2):
                  ytmp = np.append(ytmp,0.5*(y[jj]+y[jj+1]))
            if len(ytmp) != 0:
                ys[ii,n] = np.max(ytmp)
            else:
                ys[ii,n] = 0

        return ys
