import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from cluster_it_noprint import cluster_it
import scipy.io as sio

#########################################################
# Specify the path for the cases and the case name.
# Choose to save or not.
#########################################################
pathexp='database/TBL_experiment/'
mycase='Adrian_Low'
save=1

nice_fonts = {
        # Use LaTex to write all text
        "text.usetex": False,
        "font.family": "serif",
        "mathtext.fontset": "dejavuserif",
        # Use 12pt font in plots, to match 12pt font in document
        "axes.labelsize": 12,
        "font.size": 12,
        # Make the legend/label fonts a little smaller
        "legend.fontsize": 11,
        "xtick.labelsize": 11,
        "ytick.labelsize": 11}

mpl.rcParams.update(nice_fonts)

#########################################################
# Function to calculate the TKE.
#########################################################
def TKE_calc(vel_x,vel_y,u_inf):
    TKE_mat = np.zeros(np.shape(vel_x))
    
    TKE_mat[1:-1,1:-1] = (vel_x[:-2,1:-1] - u_inf)**2 + \
        (vel_x[2:,1:-1] - u_inf)**2 + \
        (vel_x[1:-1,1:-1] - u_inf)**2 + \
        (vel_x[:-2,:-2] - u_inf)**2 + \
        (vel_x[2:,:-2] - u_inf)**2 + \
        (vel_x[1:-1,:-2] - u_inf)**2 + \
        (vel_x[:-2,2:] - u_inf)**2 + \
        (vel_x[2:,2:] - u_inf)**2 + \
        (vel_x[1:-1,2:] - u_inf)**2 + \
        vel_y[:-2,1:-1]**2 + vel_y[1:-1,1:-1]**2 + vel_y[2:,1:-1]**2 + \
        vel_y[:-2,:-2]**2 + vel_y[1:-1,:-2]**2 + vel_y[2:,:-2]**2 + \
        vel_y[:-2,2:]**2 + vel_y[1:-1,2:]**2 + vel_y[2:,2:]**2

    return TKE_mat*100/9/u_inf**2

mycases=['UofA','Adrian_High','Adrian_Low']
colors=['k','b','m']
symbols=['o','x','^']

# Covering all 3 cases
for q in range(len(mycases)):
    mycase = mycases[q]
    print(mycase)
    if mycase=='UofA':
        delta = 84.
        Uinf = 11.8

        # Loading the files
        case='UofA/fullDataSet.mat'
        filename = pathexp+case
        mat_contents = sio.loadmat(filename)
        x = mat_contents['X'][0][2:-2][::-1] 
        x = x + x.max()
        y = mat_contents['Y'][::-1].T[0]
        X,Y=np.meshgrid(x,y,indexing='ij')

        # Nb of grid points
        Nx = len(x)
        Ny = len(y)

        # Choose number of snapshots
        N = 1000
        Uall = mat_contents['U_set'][::-1,2:-2,:N].T
        Vall = mat_contents['V_set'][::-1,2:-2,:N].T
        
        # Divisions for conditional averaging
        div = 40
        ylim = -0.3
        yrange = np.linspace(ylim,ylim+0.012*(div-1),div)

    if mycase=='Adrian_High':
        delta = 83.1
        Uinf = 11.39

        # Snapshots, nb of grid points
        N = 48
        Nx = 216
        Ny = 86

        # Divisions for conditional averaging
        div = 40
        ylim = -0.3
        yrange = np.linspace(ylim,ylim+0.015*(div-1),div)

    if mycase=='Adrian_Low':
        delta = 76.2
        Uinf = 1.77

        # Snapshots, nb of grid points
        N = 43
        Nx = 216
        Ny = 90

        # Divisions for conditional averaging
        div = 40
        ylim = -0.3
        yrange = np.linspace(ylim,ylim+0.015*(div-1),div)


    #########################################################
    # Global parameters.
    #########################################################
    intermit_plot = True
    cond_plot = True
    counter = 0.
    clusters = 2
    times = 2

    # Initialization
    y_coords = []
    y_coords_FCM = []
    U_cond = np.zeros((div,Nx))
    U_cond_FCM = np.zeros((div,Nx))
    glob = np.zeros((Nx,Ny))
    glob_FCM = np.zeros((Nx,Ny))

    # Looping around all the snapshots
    for i in range(1,N+1):
        print(i)
        if mycase=='UofA':
            U = mat_contents['U_set'][::-1,2:-2,i].T
            V = mat_contents['V_set'][::-1,2:-2,i].T
            thres = 3.0
        if mycase=='Adrian_High':
            if i < 10:
                case = 'AMT00High' + str(0) + str(i) + '.dat'
            else:
                case = 'AMT00High' + str(i) + '.dat'
            filename=pathexp+'AMT00HighReAll/'+case
            X,Y,U,V = np.loadtxt(filename,unpack=True,skiprows=1,delimiter=',')
            X = np.reshape(X,(86,216)).T
            Y = np.reshape(Y,(86,216)).T
            U = np.reshape(U,(86,216)).T
            V = np.reshape(V,(86,216)).T
            x = X[:,2]
            y = Y[0,:]
            thres = 6.0
        if mycase=='Adrian_Low':
            if i < 10:
                case = 'AMT00Low' + str(0) + str(i) + '.dat'
            else:
                case = 'AMT00Low' + str(i) + '.dat'
            filename=pathexp+'AMT00LowReAll/'+case
            X,Y,U,V = np.loadtxt(filename,unpack=True,skiprows=1,delimiter=',')
            X = np.reshape(X,(90,216)).T
            Y = np.reshape(Y,(90,216)).T
            U = np.reshape(U,(90,216)).T
            V = np.reshape(V,(90,216)).T
            x = X[:,2]
            y = Y[0,:]
            thres = 32.5

        # Iterative clustering
        trial = cluster_it(x,y,U,clusters,times)

        # Calculating the TKE
        TKE = TKE_calc(U,V,Uinf)
        
        # Setting the threshold
        dummy = TKE >= thres*Uinf/100

        # Converting array from True/False to Binary
        dummy = np.multiply(dummy,1.)

        # Appending global array
        glob += dummy

        # FCM method
        temp = trial.clustSum == 1.
        temp = np.multiply(temp,1.)
        
        # Only append if we are converging
        if trial.fp2 > trial.fp1:
            counter += 1
            glob_FCM += temp

        # Loop around x-axis
        for j in range(1,len(x)-1):
            # Find location of TNTI from TKE
            loc_y = np.where(dummy[j] < 0.505)
            z = y[loc_y][1]
            y_coords.append(z/delta)

            # Find location of TNTI from FCM
            if trial.fp2 > trial.fp1:
                loc_y_FCM = np.where(temp[j] < 0.505)
                if len(loc_y_FCM[0]) == 0: 
                    z_FCM = y[-1]
                else:
                    z_FCM = y[loc_y_FCM][1]
                y_coords_FCM.append(z_FCM/delta)
            
            # Loop around all y-divisions
            # Calculate conditional averaging for TKE and FCM
            for k in range(len(yrange)):
                # Below TNTI
                if yrange[k] < 0: 
                    # Find first value = right underneath interface
                    val = np.where((y - z)/delta >= yrange[k])[0][0]
                    if trial.fp2 > trial.fp1:
                        val_FCM = np.where((y - z_FCM)/delta >= yrange[k])[0][0]
                # Above TNTI
                else:
                    # Find last value = right above interface
                    val = np.where((y - z)/delta <= yrange[k])[0][-1]
                    if trial.fp2 > trial.fp1:
                        val_FCM = np.where((y - z_FCM)/delta <= yrange[k])[0][-1]
                U_cond[k][j] += U[j][val]
                if trial.fp2 > trial.fp1:
                    U_cond_FCM[k][j] += U[j][val_FCM]

    # Finding the mean
    U_cond = U_cond/N
    glob = glob/N
    intermit = np.mean(glob[1:-1,1:-1],axis=0)
    U_cond_FCM = U_cond_FCM/counter
    glob_FCM = glob_FCM/counter
    intermit_FCM = np.mean(glob_FCM[1:-1,1:-1],axis=0)

    # Calculating the mean and std of all the TNTI heights
    mean_y = np.mean(y_coords)
    std_y = np.std(y_coords)
    mean_y_FCM = np.mean(y_coords_FCM)
    std_y_FCM = np.std(y_coords_FCM)

    # Calculating the mean of the conditionally averaged velocity
    U_cond_mean = np.mean(U_cond,axis=1)
    U_cond_mean_FCM = np.mean(U_cond_FCM,axis=1)

    print('--------------------------------------------------------------------')
    print('Mean location of TNTI for', mycase, 'is', np.round(mean_y,2), 'delta')
    print('Std of TNTI for', mycase, 'is', np.round(std_y,2), 'delta')
    print('Sum of 3 mu and 3 sigma is', np.round(mean_y + 3*std_y,2))
    print('--------------------------------------------------------------------')
    print('TKE threshold is', np.round(thres*Uinf/100,2))
    print('')

    if intermit_plot:
        plt.figure(1, figsize=(5,4))
        plt.plot(y[1:-1]/delta,intermit,':',color=colors[q],linewidth=1.0)
        plt.plot(y[1:-1]/delta,intermit_FCM,'-',color=colors[q],linewidth=0.75)
        plt.xlim(0,1)
        plt.ylim(0,1.05)
        plt.xlabel('y/$\\delta$')
        plt.ylabel('$\\gamma$')
        plt.tight_layout()

    if cond_plot:
        plt.figure(2, figsize=(5,4))
        plt.plot(U_cond_mean_FCM/Uinf, yrange,'-k',linewidth=0.75,mew=0.75,ms=6,marker=symbols[q],mfc='none')
        plt.plot([-1, 1], [0.,0.], linewidth=0.5, color='grey',dashes=(10,5))
        plt.xlabel('$\\langle U\\rangle$/U$_{\\infty}$')
        plt.ylabel('$(y - y_{i})/\\delta$')
        plt.ylim(-0.1,0.1)
        plt.yticks([-0.1, -0.05, 0, 0.05, 0.1])
        plt.xlim(0.84,0.965)
        plt.xticks([0.84,0.87,0.9,0.93,0.96])
        plt.tight_layout()

if save==1:
    plt.figure(1)
    plt.savefig('figs/intermit2.png', bbox_inches='tight', dpi=600)
    plt.figure(2)
    plt.savefig('figs/Cond2.png', bbox_inches='tight', dpi=600)

plt.show()
